#!/bin/sh

set -o nounset -x

groupId=david.bravo.atmira
artifactId=demo

archetypeArtifactId=maven-archetype-quickstart
archetypeVersion=1.4
interactiveMode=false

mvn archetype:generate -DgroupId=${groupId} -DartifactId=${artifactId} -DarchetypeArtifactId=${archetypeArtifactId} -DarchetypeVersion=${archetypeVersion} -DinteractiveMode=${interactiveMode}

mv -v ${artifactId}/* .

rmdir ${artifactId}

mvn package

java -cp target/${artifactId}-1.0-SNAPSHOT.jar ${groupId}.App
